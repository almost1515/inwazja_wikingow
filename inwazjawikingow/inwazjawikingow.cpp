// inwazjawikingow.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include "Talia.h"
#include "Sterowanie.h"
#include "Wyglad.h"
#include <iostream>
#include <string>
struct XY{
	int x;
	int y;
};
XY wspolrzedne[11] = { { 27, 2 }, { 27, 5 }, { 27, 8 }, { 27, 11 }, { 27, 14 }, { 27, 17 }, { 27, 20 }, { 5, 8 }, { 16, 8 }, { 38, 8 }, { 49, 8 } };
XY reszta = { 7, 21 };
XY dyspozycja = { 43, 21 };
XY pole1 = wspolrzedne[7];
XY pole2 = wspolrzedne[6];
XY pole3 = wspolrzedne[10];
XY obecne = wspolrzedne[6];
int pozycja = 1;
unsigned char znak;
bool zakryta = false;
bool zaznaczona = false;
char strza趾i = ' ';
int lewa = 7;
int prawa = 10;
int dol = 6;
bool koniec = false;

void zczytaj();
using namespace std;
int _tmain(int argc, _TCHAR* argv[])
{
	int wykorzystane = 0;
	Talia Rozdanie;
	Wyglad Plansza;
	Sterowanie Odczyt;
	int licznik = 1;
	Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[lewa]);
	int wartosc1 = Rozdanie.Current->wartosc_karty;
	Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[dol]);
	int wartosc2 = Rozdanie.Current->wartosc_karty;
	Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[prawa]);
	int wartosc3 = Rozdanie.Current->wartosc_karty;
	Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.pozostale_karty[0]);
	int wartosc4 = Rozdanie.Current->wartosc_karty;


	for (int i = 0; i < 11; i++)
	{
		Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[i]);
		Plansza.WypelnianieKarty(wspolrzedne[i].x, wspolrzedne[i].y, Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
	}
	Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.pozostale_karty[wykorzystane]);
	Plansza.WydajZPozosta造ch(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
	Plansza.ZaznaczKarte(wspolrzedne[6].x, wspolrzedne[6].y);
	while (Odczyt.koniec == false)
	{
		Rozdanie.rozdane;
		Rozdanie.pozostale_karty;
		Odczyt.zczytaj();
		Odczyt.pozycja;
		Odczyt.zakryta;
		Odczyt.zaznaczona;
		Odczyt.strza趾i;
		if (lewa == 9 && Odczyt.pozycja == 1)
			Odczyt.pozycja = 2;
		if (prawa == 8 && Odczyt.pozycja == 3)
			Odczyt.pozycja = 2;
		if (lewa == 9 && prawa == 2 && dol == -1)
			Plansza.Wygrana();
		if (Odczyt.pozycja == 1 && Odczyt.strza趾i != 'd')
		{
			Plansza.OdznaczKarte(obecne.x, obecne.y);
			Plansza.ZaznaczKarte(pole1.x, pole1.y);
			obecne = pole1;
			if (Odczyt.zaznaczona == true && (wartosc4 == wartosc1 - 1 || wartosc4 == wartosc1 + 1))
			{
				Plansza.UsuwanieKarty(pole1.x, pole1.y);
				Plansza.OdznaczKarte(pole1.x, pole1.y);
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[lewa]);
				Plansza.WydajZPozosta造ch(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
				lewa += 1;
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[lewa]);
				wartosc4 = wartosc1;
				wartosc1 = Rozdanie.Current->wartosc_karty;
				pole1 = wspolrzedne[lewa];
				Odczyt.zaznaczona = false;
				if (lewa == 8)
				{
					Plansza.ZaznaczKarte(pole1.x, pole1.y);
					Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[lewa]);
					obecne = pole1;
				}
				else
				{
					Plansza.ZaznaczKarte(pole2.x, pole2.y);
					obecne = pole2;
				}
			}
		}
		if (Odczyt.pozycja == 2 && Odczyt.strza趾i != 'd')
		{
			Plansza.OdznaczKarte(obecne.x, obecne.y);
			Plansza.ZaznaczKarte(pole2.x, pole2.y);
			obecne = pole2;
			if (Odczyt.zaznaczona == true && (wartosc4 == wartosc2 - 1 || wartosc4 == wartosc2 + 1))
			{
				Plansza.UsuwanieKarty(pole2.x, pole2.y);
				Plansza.OdznaczKarte(pole2.x, pole2.y);
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[dol]);
				Plansza.WydajZPozosta造ch(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
				dol -= 1;
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[dol]);
				wartosc4 = wartosc2;
				wartosc2 = Rozdanie.Current->wartosc_karty;
				pole2 = wspolrzedne[dol];
				Odczyt.zaznaczona = false;
				if (dol >= 0)
				{
					Plansza.ZaznaczKarte(pole2.x, pole2.y);
					Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[dol]);
					obecne = pole2;
				}
				else if (prawa > 9)
				{
					Plansza.ZaznaczKarte(pole2.x, pole2.y);
					obecne = pole2;
				}

			}
		}
		if (Odczyt.pozycja == 3 && Odczyt.strza趾i != 'd')
		{
			Plansza.OdznaczKarte(obecne.x, obecne.y);
			Plansza.ZaznaczKarte(pole3.x, pole3.y);
			obecne = pole3;
			if (Odczyt.zaznaczona == true && (wartosc4 == wartosc3- 1 || wartosc4 == wartosc3 + 1))
			{
				Plansza.UsuwanieKarty(pole3.x, pole3.y);
				Plansza.OdznaczKarte(pole3.x, pole3.y);
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[prawa]);
				Plansza.WydajZPozosta造ch(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
				prawa -= 1;
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[prawa]);
				wartosc4 = wartosc3;
				wartosc3 = Rozdanie.Current->wartosc_karty;
				pole3 = wspolrzedne[prawa];
				Odczyt.zaznaczona = false;
				if (prawa == 9)
				{
					Plansza.ZaznaczKarte(pole3.x, pole3.y);
					Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.rozdane[prawa]);
					obecne = pole3;
				}
				else
				{
					Plansza.ZaznaczKarte(pole2.x, pole2.y);
					obecne = pole2;
				}
			}

		}
		if (Odczyt.strza趾i == 'd')
		{
			Plansza.OdznaczKarte(obecne.x, obecne.y);
			Plansza.ZaznaczKarte(reszta.x, reszta.y);
			obecne = reszta;
			if (Odczyt.zaznaczona = true && wykorzystane < 31)
			{
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.pozostale_karty[wykorzystane]);
				Plansza.WydajZPozosta造ch(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
				wykorzystane += 1;
				wartosc4 = Rozdanie.Current->wartosc_karty;
				Odczyt.zaznaczona = false;
				
			}
		}
	}
	return 0;
}

