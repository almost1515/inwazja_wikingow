#include "stdafx.h"
#include "Karta.h"
#include <iostream>
using namespace std;
void Karta::WstawNaPoczatek(Karta* &Head, Karta* &Tail, int id, int wartosc, string kolor, string znak)
{
	Karta * nowy = new Karta();
	nowy->id_karty = id;
	nowy->wartosc_karty = wartosc;
	nowy->kolor_karty = kolor;
	nowy->znak_karty = znak;
	if (Head == nullptr)
	{
		nowy->Next = nullptr;
		nowy->Prev = nullptr;
		Tail = nowy;

	}
	else
	{
		Head->Prev = nowy;
		nowy->Next = Head;
	}
	Head = nowy;

}
void Karta::WstawNaKoniec(Karta* &Tail, int id, int wartosc, string kolor, string znak)
{
	Karta * nowy = new Karta();
	nowy->id_karty = id;
	nowy->wartosc_karty = wartosc;
	nowy->kolor_karty = kolor;
	nowy->znak_karty = znak;
	Tail->Next = nowy;
	nowy->Prev = Tail;
	Tail = nowy;
}
void Karta::TasujTalie(Karta* &Head, Karta* &Tail)
{
	srand(time(nullptr));
	Karta *tasowana=Head;
	Karta *temp;
	int losowe;
	for (int i = 0; i < 1000; i++)
	{
		tasowana = Head;
		losowe = rand() % 52 + 1;
		for (int j = 0; j < 51; j++){
			if (tasowana->id_karty == losowe)
				break;
			tasowana = tasowana->Next;
		}
			if (tasowana == Head)
			{
				continue;
			}
			else if (tasowana == Tail)
			{
				temp = tasowana->Prev;
				temp->Next = nullptr;
				Tail = temp;
				Head->Prev = tasowana;
				tasowana->Next = Head;
				Head = tasowana;
			}
			else
			{
				temp = tasowana->Prev;
				temp->Next = tasowana->Next;
				temp = tasowana->Next;
				temp->Prev = tasowana->Prev;
				Head->Prev = tasowana;
				tasowana->Next = Head;
				tasowana->Prev = nullptr;
				Head = tasowana;
			}
		}
}


Karta::Karta()
{
}


Karta::~Karta()
{
}
